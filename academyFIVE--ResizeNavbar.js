// ==UserScript==
// @name         academyFIVE::ResizeNavbar
// @namespace    tvog/academyfive
// @version      2024-05-13
// @description  Verbreitert das Baummenü auf der linken Seite
// @author       Tobias Vogler
// @match        https://a5.meine-hochschule.de/*
// @match        https://a5.deine-hochschule.de/*
// @icon         https://www.academyfive.com/typo3conf/ext/sitepackage/Resources/Public/build/assets/images/favicon-academyfive.ico
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    //  neue breite des menüs
    const width = "400";

    var navbar = document.getElementById("nav");

    // wurde das element gefunden, ändere die breite
    if(navbar) {
        console.log("Changing navbar width to " + width + "px...");
        navbar.style.width = width + "px";
    }
})();