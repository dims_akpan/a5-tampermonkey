// ==UserScript==
// @name         academyFIVE::ResizeSQLTextarea
// @namespace    tvog/academyfive
// @version      2024-05-13
// @description  Vergrößert das Eingabefeld für SQL-Queries von gespeicherten Suchen
// @author       Tobias Vogler
// @match        https://a5.meine-hochschule.de/*
// @match        https://a5.deine-hochschule.de/*
// @icon         https://www.academyfive.com/typo3conf/ext/sitepackage/Resources/Public/build/assets/images/favicon-academyfive.ico
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const width = "1500";
    const height = "1000";

    // finde eingabefeld mit der ID "sql"
    var sqlTextarea = document.getElementById("sql");

    // verändere das eingabefeld nur, wenn es auf der Seite "admin/profil_query_edit.php" vorkommt und vom typ textarea ist
    if(sqlTextarea && sqlTextarea.type == "textarea" && sqlTextarea.baseURI.includes("admin/profil_query_edit.php")) {
        console.log("Changing textarea dimensions to " + width + "x" + height + "px...");
        sqlTextarea.style = "width:" + width + "px;height:" + height + "px";
    }
})();